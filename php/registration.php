<?php
define('SQL_HOST', 'localhost');
define('SQL_USER', 'user');
define('SQL_PW', 'pw');
define('SQL_DB', 'db');

class Registration {
    var $name;
    var $email;
    var $message;

    function Registration($name, $email, $message) {
        if (isset($name)) {
            // TODO
            $this->name = trim($name);
        }
        if (isset($email)) {
            // TODO
            $this->email = trim($email);
        }
        if (isset($message)) {
            $this->message = $message;
        }
    }
}

function getMailBody($reg) {
    $name = $reg->name;
    if (!isset($name) || strlen(trim($name)) == 0) {
        $name = "látogató";
    }
    $body = <<<EOT
<html>
  <head>
    <title>Kreatívkupon regisztráció</title>
  </head>
  <body>

  <p>
    Kedves $name!
  </p>

  <p>
    Köszönjük, hogy regisztráltál.
  </p>

  <p>
    üdv,<br>
        kreativkupon
  </p>

  </body>
</html>
EOT;
    return $body;
}

function sendMail($reg) {
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: KreatívKupon <info@kreativkupon.hu>' . "\r\n";
    $headers .= "To: {$reg->email}" . "\r\n";

    $subject = "Regisztráció";
    $body = getMailBody($reg);

    mail($reg->email, $subject, $body, $headers);
}

function parseForm() {
    return new Registration(
            @$_REQUEST['name'],
            @$_REQUEST['email'],
            @$_REQUEST['message']
        );
}

function saveRegistration($reg) {
    $dbh = new mysqli(SQL_HOST, SQL_USER, SQL_PW, SQL_DB);
    if ($dbh->connect_errno) {
        return FALSE;
    }
    $stmt = $dbh->prepare('INSERT INTO kk_registrations (name, email, message) VALUES (:name, :email, :message)');
    if (!$stmt) {
        return FALSE;
    }
    $stmt->bindParam(':name', $reg->name);
    $stmt->bindParam(':email', $reg->email);
    $stmt->bindParam(':message', $reg->message);
    $stmt->execute();
    $stmt->close();
    $dbh->close();
    return TRUE;
}

$reg = parseForm();

if (TRUE || isset($reg->email)) {
    if (saveRegistration($reg)) {
        sendMail($reg, $thanks);
        print("FAK DB\n");
        // show success page
    }
    else {
        // show error page
    }
}
else {
    // add error info, show reg page
}
?>

