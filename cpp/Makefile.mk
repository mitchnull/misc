CXX=g++
CXXFLAGS=-Wall -I/usr/local/include
LDFLAGS=-L/usr/local/lib
OPTFLAGS=-O2
DEBUGFLAGS=-g
GMPLIBS=-lgmpxx -lgmp
MKDEP_CPP_OPTS=-MM
# PROJECT_ROOT is normally set from our magic Makefile
PROJECT_ROOT?=.
VPATH=${PROJECT_ROOT}/src
SRCS!=ls ${VPATH} | grep '\.cpp$$'
PROGS=${SRCS:R}
DEBUG_PROGS=${PROGS:S/$/_d/}
.OBJDIR=.

LIBS=$(GMPLIBS)

.MAIN: all

.SUFFIXES: .cpp .o _d _d.o _r
.NULL: _r

.o_r:
	$(CXX) $(CXXFLAGS) $(OPTFLAGS) $(LDFLAGS) $(LIBS) -o $@ $<

_d.o_d:
	$(CXX) $(CXXFLAGS) $(DEBUGFLAGS) $(LDFLAGS) $(LIBS) -o $@ $<

.cpp.o:
	$(CXX) $(CXXFLAGS) $(OPTFLAGS) -c -o $@ $<

.cpp_d.o:
	$(CXX) $(CXXFLAGS) $(DEBUGFLAGS) -c -o $@ $<


all: ${PROGS}

debug: ${DEBUG_PROGS}

clean::
	@rm -f *.o *~

distclean:: clean
	@rm -f ${PROGS}
	@rm -f ${DEBUG_PROGS}

depend::
	@(cd ${VPATH} && mkdep -f "${.CURDIR}/.depend" ${MKDEP_CPP_OPTS} ${SRCS})

