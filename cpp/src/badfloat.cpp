//#include <vector>
//#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <cmath>

namespace {
    typedef double Number;

    const double PREFDIVS = 10000;
    template<typename T>
    T
    badAbs(T x) {
        T y = std::abs(x);
        for (int i = 0; i < 128; ++i) {
            y = std::sqrt(y);
        }
        T w = y;
        for (int i = 0; i< 128; ++i) {
            w = w * w;
        }
        return w;
    }

    template<typename T>
    void
    test(int divs) {
        T a = 0;
        T b = 2;
        for (int i = 0; i <= divs; ++i) {
            T x = ((a+b) * i) / divs;
            T ax = std::abs(x);
            T bax = badAbs(x);
            if (ax != bax) {
                std::cout << x << ", " << ax << ", " << bax << ", " << std::abs(bax - ax) << "\n";
            }
        }
    }
    
    // -------------------------------------------------------------- //
}

int
main(int argc, const char **argv) {
    int divs = PREFDIVS;
    if (argc > 1) {
        divs = boost::lexical_cast<int>(argv[1]);
    }
    std::cout << "float:\n";
    test<float>(divs);
    std::cout << "double:\n";
    test<double>(divs);
    std::cout << "long double:\n";
    test<long double>(divs);
    return 0;
}
