#include <vector>
#include <algorithm>
#include <map>
#include <iostream>
#include <limits>
#include <cmath>
#include <boost/lexical_cast.hpp>

namespace {
    typedef double Number;
    typedef std::vector<Number> Vec;
    typedef std::map<Number, Number> Map;
    typedef std::pair<Number, Number> Pair;

    const Number SHIFT = 10.0;
    const int PREFDIVS = 5;
    const int MINDIVS = 4;
    const Number NORMTRESHOLD = 2.0; 
    
    void
    writeVec(std::ostream &os, const Vec &v) {
        bool isFirst = true;
        for (Vec::const_iterator i = v.begin(); i != v.end(); ++i) {
            if (isFirst) {
                isFirst = false;
            }
            else {
                os << ' ';
            }
            os << *i;
        }
    }
    
    Vec
    readVec(std::istream &in) {
        Vec res;
        Vec::value_type tmp;
        while (in >> tmp) {
            res.push_back(tmp);
        }
        in.clear();
        in.get();
        return res;
    }

    // -------------------------------------------------------------- //

    Number shift(Number n, int shifts) {
        for (int i = 0; i < shifts; ++i) {
            n *= SHIFT;
        }
        for (int i = 0; i > shifts; --i) {
            n /= SHIFT;
        }
        return n;
    }

    struct MinMaxDivs {
        Number min;
        Number max;
        int shifts;
        int divs;

        MinMaxDivs(const Number &min, const Number &max, int shifts = 0, int divs = 0) {
            this->min = min;
            this->max = max;
            this->shifts = shifts;
            this->divs = divs;
        }

        friend
        std::ostream &
        operator<<(std::ostream &os, const MinMaxDivs &mmd) {
            return os << "[min=" << mmd.min << ", max=" << mmd.max << ", divs=" << mmd.divs << ", shifts=" << mmd.shifts << "]";
        }

        Number
        minValue() {
            return shift(min, -shifts);
        }

        Number
        maxValue() {
            return shift(max, -shifts);
        }

        Number
        delta() {
            return (maxValue() - minValue()) / divs;
        }
    };
    
    // -------------------------------------------------------------- //

    class GraphDivs {

    private:
        int prefDivs_;
        int minDivs_;

        Number ceil(Number n, int ticks) {
            return ticks * std::ceil(n / ticks);
        }

        Number floor(Number n, int ticks) {
            return ticks * std::floor(n / ticks);
        }

        int abs(int a) {
            if (a < 0) {
                return -a;
            }
            else {
                return a;
            }
        }

        MinMaxDivs
        better(const MinMaxDivs &a, const MinMaxDivs &b) {
            if (b.divs != 0 
                    && (a.divs == 0 
                    || b.divs >= minDivs_
                    && (a.divs < minDivs_
                    || (abs(b.divs - prefDivs_) < abs(a.divs - prefDivs_))))) {
                return b;
            }
            else {
                return a;
            }
        }

        MinMaxDivs
        calcMinMaxDivs(MinMaxDivs mmd, int ticks) {
    //        std::cout << "calcMinMaxDivs(" << mmd << ", " << ticks << ") = ";
            mmd.min = floor(mmd.min, ticks);
            mmd.max = ceil(mmd.max, ticks);
            mmd.divs = (mmd.max - mmd.min) / ticks;
    //        std::cout << mmd << "\n";
            return mmd;
        }

        MinMaxDivs
        calcMinMaxDivs(MinMaxDivs orig) {
            MinMaxDivs res = calcMinMaxDivs(orig, 1);
            res = better(res, calcMinMaxDivs(orig, 2));
            res = better(res, calcMinMaxDivs(orig, 5));
            res = better(res, calcMinMaxDivs(orig, 15));
            return res;
        }

    public:
        GraphDivs(int prefDivs = PREFDIVS, int minDivs = MINDIVS)
            : prefDivs_(prefDivs), minDivs_(minDivs) {
        }

        MinMaxDivs
        calcMinMaxDivs(Number min, Number max) {
            if (min == max) {
                // FIXME
                return calcMinMaxDivs(min - 1, max + 1);
            }
            MinMaxDivs res = calcMinMaxDivs(MinMaxDivs(min, max));
            if (max - min > prefDivs_) {
                int shifts = 0;
                while (max - min > prefDivs_) {
                    min = shift(min, -1);
                    max = shift(max, -1);
                    --shifts;
                    res = better(res, calcMinMaxDivs(MinMaxDivs(min, max, shifts)));
                }
            }
            else if (max - min < prefDivs_) {
                int shifts = 0;
                while (max - min < prefDivs_) {
                    min = shift(min, 1);
                    max = shift(max, 1);
                    ++shifts;
                    res = better(res, calcMinMaxDivs(MinMaxDivs(min, max, shifts)));
                }
            }
            return res;
        }

        Vec
        calcDivs(const Vec &v, int prefDivs = PREFDIVS, int minDivs = MINDIVS) {
            Vec res;
            if (v.empty()) {
                return  res;
            }
            Number min = v[0];
            Number max = min;
            for (Vec::size_type i = 1; i < v.size(); ++i) {
                Number t = v[i];
                if (t < min) {
                    min = t;
                }
                if (t > max) {
                    max = t;
                }
            }
            MinMaxDivs mmd = calcMinMaxDivs(min, max);
    //        std::cout << "result: " << mmd << "\n";

            min = mmd.minValue();
            max = mmd.maxValue();
            Number delta = mmd.delta();
            for (int i = 0; i <= mmd.divs; ++i) {
                Number div = min + (i * delta);
                res.push_back(div);
            }
            return res;
        }
    };

    // -------------------------------------------------------------- //
}

int
main(int argc, const char **argv) {
    int prefDivs = PREFDIVS;
    int minDivs = MINDIVS;
    if (argc > 1) {
        prefDivs = boost::lexical_cast<int>(argv[1]);
    }
    if (argc > 2) {
        minDivs = boost::lexical_cast<int>(argv[2]);
    }
    std::cout << "prefDivs: " << prefDivs << "\n";
    std::cout << "minDivs: " << minDivs << "\n";
    GraphDivs gd(prefDivs, minDivs);
    if (argc > 3) {
        int n = boost::lexical_cast<int>(argv[3]);
        for (int i = -n; i <= n; ++i) {
            for (int j = i + 1; j <= n; ++j) {
                Vec v;
                v.push_back(i);
                v.push_back(j);
                Vec res = gd.calcDivs(v);
                std::cout << (res.size() - 1) << ": ";
                writeVec(std::cout, v);
                std::cout << " => ";
                writeVec(std::cout, res);
                std::cout << "\n";
            }
        }
        return 0;
    }

    while (true) {
        Vec v = readVec(std::cin);
        if (v.size() == 0) {
            break;
        }
        Vec divs = gd.calcDivs(v);
        std::cout << gd.calcMinMaxDivs(divs.front(), divs.back()) << "\n";
        writeVec(std::cout, divs);
        std::cout << "\n";
    }
    return 0;
}
